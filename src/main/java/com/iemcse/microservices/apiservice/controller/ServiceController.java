package com.iemcse.microservices.apiservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iemcse.microservices.apiservice.ApiServiceApplication;
import com.iemcse.microservices.apiservice.service.ApiRestService;

@RestController
public class ServiceController {

	@Autowired
	ApiRestService apiRestService;
	
	@RequestMapping(method = RequestMethod.GET, path = "/hello-world")
	public String helloWorld() {
		return apiRestService.getNewResource();
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/hello-world")
	public String putHelloWorld() {
		return apiRestService.addNewResource();
	}
	
}
