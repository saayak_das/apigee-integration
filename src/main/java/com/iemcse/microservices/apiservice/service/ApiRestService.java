package com.iemcse.microservices.apiservice.service;

import org.springframework.stereotype.Service;

@Service
public class ApiRestService {
	
	public String getNewResource() {
		return "New Resource --- branch version";
	}
	
	public String addNewResource() {
		return "Added";
	}

}
